package com.ucbcba;

public class Square implements Figure{
    int width;
    Square(int width){
        this.width=width;
    }

    @Override
    public double perimeter() {
        return width*4;
    }

    @Override
    public double area() {
        return width*width;
    }

    @Override
    public void draw() {
        System.out.println(" perimetro del cuadrado:"+perimeter());
        System.out.println("area del cuadrado:"+area());
        for(int i=0;i<width;i++)
        {
            for(int j=0;j<width;j++)
                System.out.print('*');
            System.out.print("\n");
        }
    }
}
