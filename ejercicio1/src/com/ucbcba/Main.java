package com.ucbcba;

public class Main {
    public static void main(String[]args){
        Figure circle=new Circle(4);
        Figure square=new Square(4);
        draw(square);
        draw(circle);
    }
    public static void draw(Drawable drawable) {
        if(drawable.getClass()==Square.class){
            System.out.println("es un cuadrado");
        }
        if(drawable.getClass()==Circle.class){
            System.out.println("es un circulo");
        }
        drawable.draw();
    }
}
