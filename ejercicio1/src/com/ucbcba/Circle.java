package com.ucbcba;

public class Circle implements Figure {
    double radius;
    Circle(double radius){
        this.radius=radius;
    }
    @Override
    public double perimeter() {
        return 2*Math.PI*radius;
    }

    @Override
    public double area() {
        return Math.PI*radius*radius;
    }

    @Override
    public void draw() {
        System.out.println("perimetro del ciruclo"+perimeter());
        System.out.println("area del ciruclo"+area());
    }
}
